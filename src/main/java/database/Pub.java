package database;

public class Pub {
   double pubid;
   String name;
   String contact;
   String phone;

   public Pub(String name,String contact,String phone) {
      this.phone=phone;
      this.name=name;
      this.contact=contact;
      this.pubid = 0;
 }
public Pub(double pubid,String name,String contact,String phone) {
      this(name,contact,phone);
      this.pubid = pubid;
   }

   @Override
   public String toString() {
      return"\n"+ name + " " + phone + " " + pubid + " " + contact ;
   }
}

