package database;

/**
 * JustLeeServices
 */
import java.sql.*;
import java.util.Scanner;
import java.sql.Date;
public class JustLeeServices {
    public static Connection getConnection(String user,String password)  {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211: 1521/pdbora19c.dawsoncollege.qc.ca", user, password);
            conn.setAutoCommit(false);
            System.out.println("DB connected");
        }
        catch(SQLException e){
            System.out.println("Connection failed");
            e.printStackTrace();
        }
        return conn;
    }
    public static Book getBook(String isbn_in,Connection conn){
        
        try {
            Statement connStatement = conn.createStatement();
            String query = "SELECT ISBN, TITLE, PUBDATE, PUBID, COST, RETAIL, NVL(DISCOUNT,0), CATEGORY FROM BOOKS WHERE ISBN LIKE "+"'"+isbn_in+"'";
            String query_pub = "SELECT * FROM PUBLISHER WHERE PUBID LIKE ";
            ResultSet rs = connStatement.executeQuery(query);
            System.out.println("Fetch Book Succesfully");
            rs.next();
            double pubId=rs.getDouble("PUBID");
            String title = rs.getString("TITLE");
            String isbn = rs.getString("ISBN");
            String category = rs.getString("CATEGORY");
            double retail = rs.getDouble("RETAIL");
            double cost =  rs.getDouble("COST");
            double discount = rs.getDouble("NVL(DISCOUNT,0)");
            String pubdate = rs.getString("PUBDATE");
            ResultSet rs_pub = connStatement.executeQuery(query_pub+pubId);
            rs_pub.next();
            System.out.println("Fetch Publisher Succefully !");
            Pub newPublisher = new Pub(rs_pub.getDouble("PUBID"),rs_pub.getString("NAME"),rs_pub.getString("CONTACT"),rs_pub.getString("PHONE"));
            Book newBook = new Book(title, isbn, category, retail, discount, cost, pubdate, newPublisher);
            conn.close();
            return newBook;
        }
        catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
    public static void addBook(Book newBook,Connection conn){
        try {
            Statement connStatement = conn.createStatement();
            String query_pub = "SELECT * FROM PUBLISHER";
            ResultSet rs_pub = connStatement.executeQuery(query_pub);
            boolean pubExisted = false;
            while(rs_pub.next()){
                if(newBook.publisher.pubid==rs_pub.getDouble("PUBID")){
                    pubExisted = true;
                    break;
                }
            }
            if(!pubExisted){
                String name = newBook.publisher.name;
                String contact = newBook.publisher.contact;
                String phone = newBook.publisher.phone;
                ResultSet maxId = connStatement.executeQuery("SELECT MAX(PUBID) FROM PUBLISHER");
                maxId.next();
                double pubid = maxId.getDouble("MAX(PUBID)")+1;
                newBook.publisher.pubid=pubid;
                connStatement.executeUpdate("INSERT INTO PUBLISHER (PUBID,NAME,CONTACT,PHONE) VALUES("+pubid+",'"+name+"','"+contact+"','"+phone+"')");
            }
            ResultSet rs = connStatement.executeQuery("SELECT ISBN FROM BOOKS");
            while(rs.next()){
                if(newBook.isbn.equals(rs.getString("ISBN"))){
                    throw new IllegalArgumentException();
                }
            }
            System.out.println(newBook.pubdate);
            connStatement.executeUpdate("INSERT INTO BOOKS (TITLE,ISBN,CATEGORY,RETAIL,DISCOUNT,COST,PUBDATE,PUBID) VALUES('"+newBook.title+"','"+newBook.isbn+"','"+newBook.category+"',"+newBook.retail+","+newBook.discount+","+newBook.cost+",TO_DATE('"+newBook.pubdate+"','YYYY-MM-DD')"+","+newBook.publisher.pubid+")");
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e){
            System.out.println("Book's already existed");
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Pub newPub = new Pub("AIDS AID", "JUAN HERRERO", "800-555-8284");
        Book newBook = new Book("BUUUUUUUS", "0666640733", "FAMILY LIFE", 40, 10, 1, "2005-10-10", newPub );
        System.out.println("Enter your username");
        String user = sc.nextLine();
        System.out.println("Enter your password");
        String password = sc.nextLine();
        sc.close();
        Connection conn = getConnection(user, password);
        addBook(newBook, conn);
        conn = getConnection(user, password);
        Book newBookGet = getBook("0666640733", conn);
        System.out.println(newBookGet);
    }
}