package database;

import java.sql.Date;


public class Book {
    String title;
    String isbn;
    String category;
    double retail;
    double discount;
    double cost;
    String pubdate;
    Pub publisher;
    public Book(String title,String isbn,String category,double retail,double discount,double cost,String pubdate,Pub newPublisher) {
        this.title = title;
        this.isbn = isbn;
        this.category = category;
        this.retail = retail;
        this.discount=discount;
        this.cost=cost;
        this.pubdate = pubdate;
        this.publisher = newPublisher;
    }
    @Override
    public String toString() {
        return "Title:" + title + "  ISBN:" + isbn + "  Ctgr:" + category + "  Retail:" + retail + "  Discount:" + discount + "  Cost:" + cost + "  Pubdate:" + pubdate + " Publisher:"+publisher;
    }
}
